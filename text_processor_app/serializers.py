from rest_framework import serializers


class SummarizeTextSerializer(serializers.Serializer):
    """
    Сериалайзер для получения суммаризованного текста
    """
    token = serializers.CharField(max_length=50)
    text = serializers.CharField()
    sentences_count = serializers.IntegerField()


class CheckSimilaritySerializer(serializers.Serializer):
    """
    Сериалайзер для сравнения похожести текстов
    """
    token = serializers.CharField(max_length=50)
    text_1 = serializers.CharField()
    text_2 = serializers.CharField()


class MakeEmbeddingsSerializer(serializers.Serializer):
    """
    Сериалайзер для вьюхи создающей эмбединги
    """
    token = serializers.CharField(max_length=50)
    text = serializers.CharField()

