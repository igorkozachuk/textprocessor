from project.settings import LOGGER, TOKEN
from rest_framework.response import Response
from rest_framework import status


class CheckRequestService:
    """
    Сервис для проверки запросов.
    """

    @staticmethod
    def check_token(token: str) -> bool:
        if not token or token != TOKEN:
            LOGGER.warning(f'Неверный токен запроса: {token} != {TOKEN}')
            return False
        return True
