
from sumy.parsers.plaintext import PlaintextParser
from sumy.nlp.tokenizers import Tokenizer
from sumy.summarizers.luhn import LuhnSummarizer
from sumy.summarizers.lsa import LsaSummarizer
from sumy.summarizers.lex_rank import LexRankSummarizer
from sumy.summarizers.text_rank import TextRankSummarizer
from sumy.summarizers.sum_basic import SumBasicSummarizer
from sumy.summarizers.reduction import ReductionSummarizer
from sumy.summarizers.kl import KLSummarizer

import spacy

from project.settings import LOGGER


class TextProcessorService:
    sentences_count = 2
    nlp = spacy.load("ru_core_news_sm")

    @staticmethod
    def summarize_lsa(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = LsaSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_luh(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = LuhnSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_lex_rank(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = LexRankSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_text_rank(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = TextRankSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_sum_basic(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = SumBasicSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_reduction(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = ReductionSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def summarize_kl(text, language="russian", sentences_count=sentences_count):
        parser = PlaintextParser.from_string(text, Tokenizer(language))
        summarizer = KLSummarizer()
        summary = summarizer(parser.document, sentences_count)
        return ' '.join([str(sentence) for sentence in summary])

    @staticmethod
    def make_embeddings(text: str):
        """
        This is a method that is used to make embeddings from text.
        """
        LOGGER.info(f"Making embeddings from text: {text}")
        doc = TextProcessorService.nlp(text)
        return doc.vector

    @staticmethod
    def check_similarity(text1: str, text2: str) -> float:
        """
        This is a method that is used to check similarity between two texts.
        :param text1: string
        :param text2: string
        :return: float
        """
        LOGGER.info(f"Checking similarity between texts: {text1} and {text2}")
        doc1 = TextProcessorService.nlp(text1)
        doc2 = TextProcessorService.nlp(text2)
        return doc1.similarity(doc2)

    @staticmethod
    def get_sentences_count(text):
        doc = TextProcessorService.nlp(text)
        count = 0
        for _ in enumerate(doc.sents):
            count += 1
        return count
