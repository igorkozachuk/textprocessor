from django.apps import AppConfig


class TextProcessorAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'text_processor_app'
