from django.urls import path

from .views import SummarizeText, CheckSimilarity, MakeEmbeddings

app_name = 'text_processor_app'

urlpatterns = [
    path('summarize_text/', SummarizeText.as_view(), name='summarize_text'),
    path('check_similarity/', CheckSimilarity.as_view(), name='check_similarity'),
    path('make_ebeddings/', MakeEmbeddings.as_view(), name='make_embeddings'),
]
