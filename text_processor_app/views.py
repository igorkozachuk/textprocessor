from django.shortcuts import render
from django.views import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from drf_spectacular.utils import extend_schema

from .services.text_processor_service import TextProcessorService
from .services.check_request_service import CheckRequestService
from .serializers import SummarizeTextSerializer, CheckSimilaritySerializer, MakeEmbeddingsSerializer
from project.settings import LOGGER


class SummarizeText(APIView):

    @extend_schema(request=SummarizeTextSerializer, responses=dict, methods=['post'])
    def post(self, request):
        LOGGER.info(f'Получен запрос на SummarizeText: {request.data}')
        token = request.data.get("token")
        if not CheckRequestService.check_token(token):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='invalid token')

        serializer = SummarizeTextSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        text = request.data.get("text")
        sentences_count = request.data.get('sentences_count')
        if not sentences_count:
            sentences_count = TextProcessorService.sentences_count

        check_sentences_count = TextProcessorService.get_sentences_count(text)
        if check_sentences_count <= sentences_count:
            return Response(
                data={'summarized_text': text,
                      'description': 'количество предложений в тексте равно тому количеству до которого вы хотите '
                                     'его сократить'},
                status=status.HTTP_200_OK
            )

        summarized_text = TextProcessorService.summarize_lex_rank(text, language="russian",
                                                                  sentences_count=sentences_count)
        return Response(data={'summarized_text': summarized_text},
                        status=status.HTTP_200_OK)


class CheckSimilarity(APIView):

    @extend_schema(request=CheckSimilaritySerializer, responses=dict, methods=['post'])
    def post(self, request):
        LOGGER.info(f'Получен запрос на CheckSimilarity: {request.data}')
        token = request.data.get("token")

        if not CheckRequestService.check_token(token):
            return Response(status=status.HTTP_400_BAD_REQUEST, data='invalid token')

        serializer = CheckSimilaritySerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        text_1 = request.data.get("text_1")
        text_2 = request.data.get("text_2")

        similarity_index = TextProcessorService.check_similarity(text_1, text_2)
        return Response(data={'similarity_index': similarity_index},
                        status=status.HTTP_200_OK)


class MakeEmbeddings(APIView):

    @extend_schema(request=MakeEmbeddingsSerializer, responses=dict, methods=['post'])
    def post(self, request):
        LOGGER.info('Получен запрос на создание эмбедингов')
        token = request.data.get('token')
        if not CheckRequestService.check_token(token):
            return Response(data='invalid token', status=status.HTTP_400_BAD_REQUEST)

        serializer = MakeEmbeddingsSerializer(data=request.data)
        if not serializer.is_valid():
            return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        text = request.data.get('text')
        embeddings = TextProcessorService.make_embeddings(text)
        return Response(data={'embeddings': embeddings}, status=status.HTTP_200_OK)
