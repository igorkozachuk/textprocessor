from django.http import HttpRequest
from django.shortcuts import redirect

from .settings import LOGGER


def redirect_to_admin(request: HttpRequest):
    """
    Вьюшка для редиректа на страницу админки.
    """
    LOGGER.info('Получен запрос на вьюшку для редиректа на страницу админки.')
    return redirect(to='/admin/')
